package com.mkyong.core;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PerroquetTest {

    @Test
    public void testPerroquetParlant() {
        // Bloc arrange
        Perroquet perroquet = new Perroquet();
        String phrase = "Ceci est un test.";

        // Bloc act
        String resultat = perroquet.perroquetParlant(phrase);

        // Bloc assert
        assertEquals(phrase, resultat);
    }

    @Test
    public void testPerroquetBonjour() {
        // Arrange
        Perroquet perroquet = new Perroquet();
        String nom = "Coco";

        // Act
        String resultat = perroquet.perroquetBonjour(nom);

        // Assert
        assertEquals("Bonjour Coco", resultat);
    }
}
