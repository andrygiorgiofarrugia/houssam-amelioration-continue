package com.mkyong.core;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Disabled;

public class GuessGameTest {
		
	private final GuessGame guessgame = new GuessGame();

	@Test
	public void testInvalidGuess() {
	    assertThat(guessgame.determineGuess(-5, 50, 1))
	            .isEqualTo("Your guess is invalid");
	    assertThat(guessgame.determineGuess(105, 50, 1))
	            .isEqualTo("Your guess is invalid");
	}

	@Test
	public void testCorrectGuess() {
	    assertThat(guessgame.determineGuess(50, 50, 3))
	            .isEqualTo("Correct!\nTotal Guesses: 3");
	}

	@Test
	public void testHighGuess() {
	    assertThat(guessgame.determineGuess(75, 50, 2))
	            .isEqualTo("Your guess is too high, try again.\nTry Number: 2");
	}

	@Test
	public void testLowGuess() {
	    assertThat(guessgame.determineGuess(25, 50, 4))
	            .isEqualTo("Your guess is too low, try again.\nTry Number: 4");
	}
	
	@Disabled
	@Test
	public void testIncorrectGuess() {
	    assertThat(guessgame.determineGuess(60, 50, 5))
	            .isEqualTo("Your guess is incorrect\nTry Number: 5");
	}
}