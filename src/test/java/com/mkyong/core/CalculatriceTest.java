package com.mkyong.core;

import org.junit.jupiter.api.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalculatriceTest {
	
	private final Calculatrice calculatrice = new Calculatrice();
	
	@BeforeAll
	public static void beforeall() {
		System.out.println("----- before all, début de tous les tests ------");
	}
	@Test
	public void testAddition() {
		Calculatrice calculatrice = new Calculatrice();
		
		//JUnit
		assertEquals(5, calculatrice.addition(2, 3));
		
		//AssertJ & Hamcrest
		assertThat(calculatrice.addition(2, 3)).isEqualTo(5);
		
	}
	@Test
	public void testSoustraction() {
		Calculatrice calculatrice = new Calculatrice();
		
		//JUnit
		assertEquals(6, calculatrice.soustraction(9, 3));
		
		//AssertJ & Hamcrest
		assertThat(calculatrice.soustraction(9, 3)).isEqualTo(6);
	}
	@Test
	public void testMultiplication() {
		Calculatrice calculatrice = new Calculatrice();
		
		//JUnit
		assertEquals(36, calculatrice.multiplication(6, 6));
		
		//AssertJ & Hamcrest
		assertThat(calculatrice.multiplication(6, 6)).isEqualTo(36);
	}
	@Test
	public void testDivision() {
		Calculatrice calculatrice = new Calculatrice();
		
		//JUnit
		assertEquals(2, calculatrice.division(6, 3));
		
		//AssertJ & Hamcrest
		assertThat(calculatrice.division(6, 3)).isEqualTo(2);
	}
	@Test
	public void testDivisionParZero() {
		Calculatrice calculatrice = new Calculatrice();
		assertNull(calculatrice.division(6, 0));
	}
	@Test
	public void testCalcul() {
		Calculatrice calculatrice = new Calculatrice();
		assertEquals(5, calculatrice.calcul(2, 3, "+"));
		assertEquals(6, calculatrice.calcul(9, 3, "-"));
		assertEquals(36, calculatrice.calcul(6, 6, "*"));
		assertEquals(2, calculatrice.calcul(6, 3, "/"));
	}
	@Test
    @Timeout(30)
    public void testWaitForSeconds() {
        assertTrue(Calculatrice.waitForSeconds(10));
    }
	@ParameterizedTest
    @CsvSource({"2, 3, 5", "0, 0, 0", "-5, 5, 0"})
    public void testAdditionWithCsvSource(int a, int b, int expectedResult) {
        assertEquals(expectedResult, calculatrice.addition(a, b));
    }
    @ParameterizedTest
	@AfterAll
	public static void afterAll() {
		System.out.println("----- Fin de tous les tests -----");
    }
}