package com.mkyong.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class FactorielTest {

    @Test
    public void testFactorielIteratif() {
        assertEquals(1, Factoriel.factorielIteratif(0));
        assertEquals(1, Factoriel.factorielIteratif(1));
        assertEquals(120, Factoriel.factorielIteratif(5));
        assertEquals(3628800, Factoriel.factorielIteratif(10));
    }

    @Test
    public void testFactorielIteratifNegatif() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Factoriel.factorielIteratif(-5);
        });
        
        assertEquals("Le nombre doit être non négatif.", exception.getMessage());
    }

    @Test
    public void testFactorielRecursifNegatif() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Factoriel.factorielRecursif(-5);
        });

        assertEquals("Le nombre doit être non négatif.", exception.getMessage());
    }
}