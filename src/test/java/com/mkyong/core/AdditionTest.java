package com.mkyong.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AdditionTest {
	@DisplayName("Test Addition.add()")
	@Test
	void testAdd(){
		assertEquals(11, Addition.add(5, 6));
	}
	
	@DisplayName("Test Addition.mult()")
	@Test
	void testMult(){
		assertEquals(4, Addition.mult(2, 2));
	}
}
