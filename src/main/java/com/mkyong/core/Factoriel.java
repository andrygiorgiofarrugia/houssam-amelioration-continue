package com.mkyong.core;

public class Factoriel {

    public static long factorielIteratif(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("Le nombre doit être non négatif.");
        }

        long resultat = 1;
        for (int i = 1; i <= n; i++) {
            resultat *= i;
        }
        return resultat;
    }

    public static long factorielRecursif(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("Le nombre doit être non négatif.");
        }

        if (n == 0 || n == 1) {
            return 1;
        } else {
            return n * factorielRecursif(n - 1);
        }
    }
}
