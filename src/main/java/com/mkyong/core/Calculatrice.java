package com.mkyong.core;

public class Calculatrice {

    public static void main(String[] args) {
        Calculatrice calc = new Calculatrice();
        int resultAdd = calc.addition(5, 3);
        int resultSub = calc.soustraction(6, 2);
        int resultMul = calc.multiplication(8, 8);
        int resultDiv = calc.division(10, 5);
        System.out.println("Résultat de l'addition : " + resultAdd);
        System.out.println("Résultat de la soustraction : " + resultSub);
        System.out.println("Résultat de la multiplication : " + resultMul);
        System.out.println("Résultat de la division : " + resultDiv);
    }
	
	public static boolean waitForSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
            return true;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return false;
        }
    }
	
	public int addition(int a, int b) {
		return a + b;
	}
	public int soustraction(int a, int b) {
		return a - b;
	}
	public int multiplication(int a, int b) {
		return a * b;
	}
	public Integer division(int a, int b) {
		if (b == 0) {
			return null;
		}
		return a / b;
	}
	public int calcul(int a, int b, String operateur) {
        switch (operateur) {
            case "+":
                return addition(a, b);
            case "-":
                return soustraction(a, b);
            case "*":
                return multiplication(a, b);
            case "/":
                return division(a, b);
            default:
                throw new IllegalArgumentException("Opérateur non valide : " + operateur);
        }
	}
}