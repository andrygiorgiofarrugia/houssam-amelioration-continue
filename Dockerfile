FROM tomcat:10.1

RUN apt-get update && apt-get install -y default-jdk

COPY target/junit5-maven-1.0.jar /usr/local/tomcat/webapps

WORKDIR /usr/local/tomcat/webapps